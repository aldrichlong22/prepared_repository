import Vue from "vue"
import App from "./App.vue"
import router from "./router"
import vuetify from "./plugins/vuetify"
import VueResource from 'vue-resource'
import VueApexCharts from 'vue-apexcharts'
import * as echarts from 'echarts'
import './api/mock.js'



Vue.config.productionTip = false
Vue.use(VueResource)
Vue.prototype.$echarts = echarts
Vue.use(VueApexCharts)
Vue.component('apexchart', VueApexCharts)

new Vue({
  router,
  data: { vulnerabilities: undefined },
  vuetify,
  render: (h) => h(App),
}).$mount("#app")


