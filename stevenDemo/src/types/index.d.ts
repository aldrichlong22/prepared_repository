export declare interface VulnerabilityReportFile {
  SchemaVersion?: number
  Results: VulnerabilityReportTarget[]
}

export type Version1OrVersion2 =
  | VulnerabilityReportTarget[]
  | VulnerabilityReportFile


export type Version1 =
  | VulnerabilityReportTarget[]


export declare interface VulnerabilityReportTarget {
  Target: string
  Type: string
  NormalInfo?: NormalInfo[]
  RefreshInfo?: RefreshInfo[]
  ChannelInfo?: ChannelInfo[]
}

export declare interface PakcageReportTarget {
  Target: string
  Type: string

  Packages?: PackageInfo[]

}

export declare interface NormalInfo {

  Channel_Name: string
  Id: number
  Channel_Name: string
  SeqNum: number
  MsgType: string
  Time: string
  Message: string
  id?: number
}

export declare interface RefreshInfo {

  Channel_Name: string
  Id: number
  Channel_Name: string
  SeqNum: number
  MsgType: string
  Time: string
  Message: string
  id?: number
}

export declare interface VulnerabilitySeverityInformation {
  severity: string
  count: number
}
export declare interface ChannelInfo {

  Channel_Name: string
  Id: number
  Channel_Name: string
  SeqNum: number
  MsgType: string
  Time: string
  Message: string
  id?: number
}